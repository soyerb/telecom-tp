%{
    TP de Télécomunications
    Avril 2024, Théo Bessel
%}

clear;
close all;

% Données de l'exercice
Rb = 3000;
Fe = 24000;

% On définit le nombre de bits de l'entrée
M = 400;

n0 = 3;

% On génère l'entrée aléatoirement (2 symbole binaire)
bits = (rand(1,M) > 0.5);

fprintf("Entrée binaire aléatoire : "); fprintf("%g", bits); fprintf("\n")

EbN0 = 0.2;

%{
    On appelle les trois chaines de transmission avec les mêmes paramètres :
%}

[kron1, z1, z_bruite1, Sz1, range1, Ns1, Mb1, ~] = chaine1(Rb, Fe, M, bits, EbN0);
[kron2, z2, z_bruite2, Sz2, range2, Ns2, Mb2, ~] = chaine2(Rb, Fe, M, bits, EbN0);
[kron3, z3, z_bruite3, Sz3, range3, Ns3, Mb3, ~] = chaine3(Rb, Fe, M, bits, EbN0);

eyediagram(z1(Ns1:end),Ns1,Ns1,Ns1-1)
fprintf("Taux d'erreur binaire de la chaine de transmission 1 sans bruit : %d \n", TEB(z1, Ns1, bits, Ns1, Mb1));
eyediagram(z2(Ns2:end),Ns2,Ns2,Ns2-1)
fprintf("Taux d'erreur binaire de la chaine de transmission 2 sans bruit : %d \n", TEB(z2, Ns2, bits, Ns2/2, Mb2));
eyediagram(z3(Ns3:end),Ns3,Ns3,Ns3-1)
fprintf("Taux d'erreur binaire de la chaine de transmission 3 sans bruit : %d \n", TEB(z3, Ns3, bits, Ns3, Mb3));

eyediagram(z_bruite1(Ns1:end),Ns1,Ns1,Ns1-1)
fprintf("Taux d'erreur binaire de la chaine de transmission 1 avec bruit : %d \n", TEB(z_bruite1, Ns1, bits, Ns1, Mb1));
eyediagram(z_bruite2(Ns2:end),Ns2,Ns2,Ns2-1)
fprintf("Taux d'erreur binaire de la chaine de transmission 2 avec bruit : %d \n", TEB(z_bruite2, Ns2, bits, Ns2/2, Mb2));
eyediagram(z_bruite3(Ns3:end),Ns3,Ns3,Ns3-1)
fprintf("Taux d'erreur binaire de la chaine de transmission 3 avec bruit : %d \n", TEB(z_bruite3, Ns3, bits, Ns3, Mb3));


EbN0_range = 0:0.01:8;
TEB1_range = zeros(1,length(EbN0_range));
TEB2_range = zeros(1,length(EbN0_range));
TEB3_range = zeros(1,length(EbN0_range));


% TEB théoriques
TEB1_th_range = zeros(1, length(EbN0_range));
TEB2_th_range = zeros(1, length(EbN0_range));
TEB3_th_range = zeros(1, length(EbN0_range));


i = 1;
for EbN0=EbN0_range

    % Calcul théoriques des courbes TEB
    TEB1_th_range(i) = 1 - normcdf(2*EbN0);
    TEB2_th_range(i) = 1 - normcdf(EbN0);
    TEB3_th_range(i) = 1 - normcdf(3*EbN0);

    [kron1, z1, z_bruite1, Sz1, range1, Ns1, Mb1, ~] = chaine1(Rb, Fe, M, bits, EbN0);
    [kron2, z2, z_bruite2, Sz2, range2, Ns2, Mb2, ~] = chaine2(Rb, Fe, M, bits, EbN0);
    [kron3, z3, z_bruite3, Sz3, range3, Ns3, Mb3, ~] = chaine3(Rb, Fe, M, bits, EbN0);
    TEB1_range(i) = TEB(z_bruite1, Ns1, bits, Ns1, Mb1);
    TEB2_range(i) = TEB(z_bruite2, Ns2, bits, Ns2, Mb2);
    TEB3_range(i) = TEB(z_bruite3, Ns3, bits, Ns3, Mb3);
    i = i+1;
end


figure
semilogy(EbN0_range, TEB1_range); hold on;
semilogy(EbN0_range, TEB2_range); hold on;
semilogy(EbN0_range, TEB3_range); hold on;
semilogy(EbN0_range, TEB1_th_range); hold on;
semilogy(EbN0_range, TEB2_th_range); hold on;
semilogy(EbN0_range, TEB3_th_range); hold off;

xlabel("Eb/N0");
ylabel("TEB");
legend('TEB expérimentale 1','TEB expérimentale 2','TEB expérimentale 3', 'TEB théorique 1', 'TEB théorique 2', 'TEB théorique 3');
title("TEB en fonction de Eb/N0 pour la chaine 1");
