%{
    TP de Télécomunications
    Avril 2024, Théo Bessel
%}

%{
    Chaine de transmission 1 :
    – Mapping : symboles binaires à moyenne nulle.
    – Filtre de mise en forme de réponse impulsionnelle rectangulaire de hauteur 1 et de durée  ́egale à la période symbole.
%}

function [kron, z, z_bruite, Sz, range, Ns, Mb, Rs, Ts] = chaine3(Rb, Fe, M, bits, EbN0)
    % On définit la période d'échantillonage
    Te = 1/Fe;

    % On effectue le mapping
    a = zeros(1, length(bits)/2);
    for i=1:length(a)
        if bits(2*i-1) == 0
            if bits(2*i) == 0
                a(i) = 3;
            else
                a(i) = 1;
            end
        else
            if bits(2*i) == 0
                a(i) = -3;
            else
                a(i) = -1;
            end
        end
    end
    %fprintf("Mapping du modulateur 1 : "); fprintf("%g", a); fprintf("\n")

    % Le nombre de bits par symboles est donc
    Mb = M/length(a);

    % On définit la période symbole
    Rs = Rb/Mb;
    Ts = 1/Rs;

    % On a donc
    Ns = floor(Ts/Te);

    % On génère un signal discontinu à partir de ce mapping
    range = 0:Te:M/Rb-Te;
    kron = zeros(1, length(range));

    for i=0:length(a)-1
        kron(i*Ns+1) = a(i+1);
    end

    % On génère ensuite un filtre
    t = 1:Ns;
    h = ones(1, length(t));
    h_r = ones(1, length(t));

    z_filtered = filter(h, 1, kron);

    z = filter(h, 1, z_filtered);

    % On ajoute du bruit
    Px = mean(abs(z_filtered).^2);
    sigma_2 = Px*Ns/(2*log2(M)*(EbN0));
    bruit = sqrt(sigma_2) * randn(1, length(z_filtered));
    z_filtered = z_filtered + bruit;

    z_bruite = filter(h_r, 1, z_filtered);

    Sz=pwelch(z, [], [], [], Fe, 'twosided');
end