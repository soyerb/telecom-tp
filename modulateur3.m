%{
    TP de Télécomunications
    Avril 2024, Théo Bessel
%}

%{
    Modulateur 3 :
    – Mapping : symboles binaires à moyenne nulle.
    – Filtre de mise en forme de réponse impulsionnelle en racine de cosinus surélevé de roll off égal à 0.5.
      Vous pouvez utiliser la fonction rcosdesign.m de Matlab afin de générer la réponse impulsionnelle
      de ce filtre. Ce filtre a une bande fréquentielle finie, il a donc une réponse impulsionnelle infinie qui
      devra être tronquée afin de réaliser un filtre de type RIF. En utilisant h = rcosdesign(alpha,L,Ns);
      vous pouvez réaliser un filtre en racine de cosinus surélevé avec une réponse impulsionnelle de
      longueur N = L * Ns + 1 échantillons (ou coefficients) et de roll off alpha (paramètre compris entre 0
      (filtre passe-bas idéal) et 1).
%}

function [kron, z, Sz, range, Ns, Mb, Rs, Ts] = modulateur3(Rb, Fe, M, bits)
    % On définit la période d'échantillonage
    Te = 1/Fe;

    % On effectue le mapping
    a = 2*bits-1;
    fprintf("Mapping du modulateur 3 : "); fprintf("%g", a); fprintf("\n")

    % Le nombre de bits par symboles est donc
    Mb = M/length(a);

    % On définit la période symbole
    Rs = Rb/Mb;
    Ts = 1/Rs;

    % On a donc
    Ns = floor(Ts/Te);

    % On génère un signal discontinu à partir de ce mapping
    range = 0:Te:M/Rb-Te;
    kron = zeros(1, length(range));

    for i=0:length(a)-1
        kron(i*Ns+1) = a(i+1);
    end

    % On génère ensuite un filtre
    alpha = 1;
    L = 10;
    h = rcosdesign(alpha, L, Ns);

    z = filter(h, 1, kron);

    Sz=pwelch(z, [], [], [], Fe, 'twosided');
end