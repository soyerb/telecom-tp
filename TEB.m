function TEB = TEB(z, Ns, bits, n0, Mb)
    z_echant = zeros(1,length(z));
    for i = n0:Ns:length(z) % n0 en param
        z_echant(i:(i+Ns-1)) = z(i);
    end

    if Mb == 1
        decisions = z_echant(Ns:Ns:end)>0; %decisions par seuil
    else
        % Decision à trois seuils pour les symboles 4-aires
        % On a la correspondance suivante :
        % 3 -> 00
        % -1 -> 11
        % 1 -> 01
        % -3 -> 10
        decisions = zeros(1,length(bits));
        for i = 1:length(bits)/2
            if z_echant(2*i:(2*i+Ns-1)) > 32
                decisions(2*i-1:2*i) = [0 0];
            elseif z_echant(2*i:(2*i+Ns-1)) > 0
                decisions(2*i-1:2*i) = [0 1];
            elseif z_echant(2*i:(2*i+Ns-1)) > -32
                decisions(2*i-1:2*i) = [1 0];
            else
                decisions(2*i-1:2*i) = [1 1];
            end
        end
    end
    
    fprintf("z_echant : "); fprintf("%g", z_echant); fprintf("\n")
    %fprintf("decisions : "); fprintf("%g", decisions); fprintf("\n")
    %fprintf("bits : "); fprintf("%g", bits); fprintf("\n")
    TEB = sum(decisions~=bits)/length(bits);
end