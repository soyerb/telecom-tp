%{
    TP de Télécomunications
    Avril 2024, Théo Bessel
%}

%{
    Modulateur 1 :
    – Mapping : symboles binaires à moyenne nulle.
    – Filtre de mise en forme de réponse impulsionnelle rectangulaire de hauteur 1 et de durée  ́egale à la période symbole.
%}

function [kron, z, Sz, range, Ns, Mb, Rs, Ts] = modulateur1(Rb, Fe, M, bits)
    % On définit la période d'échantillonage
    Te = 1/Fe;

    % On effectue le mapping
    a = 2*bits-1;
    fprintf("Mapping du modulateur 1 : "); fprintf("%g", a); fprintf("\n")

    % Le nombre de bits par symboles est donc
    Mb = M/length(a);

    % On définit la période symbole
    Rs = Rb/Mb;
    Ts = 1/Rs;

    % On a donc
    Ns = floor(Ts/Te);

    % On génère un signal discontinu à partir de ce mapping
    range = 0:Te:M/Rb-Te;
    kron = zeros(1, length(range));

    for i=0:length(a)-1
        kron(i*Ns+1) = a(i+1);
    end

    % On génère ensuite un filtre
    t = 1:Ns;
    h = ones(1, length(t));

    z = filter(h, 1, kron);

    Sz=pwelch(z, [], [], [], Fe, 'twosided');
end