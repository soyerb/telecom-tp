%{
    Chaine de transmission 3 :
    – Mapping : symboles binaires à moyenne nulle.
    – Filtre de mise en forme de réponse impulsionnelle rectangulaire de hauteur 1 et de durée  ́egale à la période symbole.

    Variables :
    - z_genere : Le signal généré à partir du mapping, avant modulation sur fréquence porteuse
    - z_emission : Le signal en sortie du modulateur, qui va être émis
    - z_bruite : Le signal à la fin du canal de transmission donc avec du bruit rajouté
    - z_reception : Le signal en sortie du récepteur (donc en sortie de la chaine de transmission)
    - z_echant : Le signal après échantillonage
    - z_demappe : Le signal après démapping
    - d : Symbole du signal après mapping
%}

function [z_kron, z_genere, z_emission, z_bruite, z_reception, z_echant, z_demappe, d, d_demappe, range, Ns, Mb, Rs, Ts] = chaine_part3(Rb, Fe, Fp, M, bits, EbN0)
    % ============ MAPPING ============

    % On effectue le mapping, par utilisation de la fonction matlab
    % pammod.m

    % regroupement des bits 2 par 2
    d_decimal = bit2int(bits', 2);

    % génération 4-ASK, phase nul.
    d = pammod(d_decimal, 4)';
    
    % ==================================
    
    % On définit la période d'échantillonage
    Te = 1 / Fe;

    % Le nombre de bits par symboles est donc
    Mb = M / length(d);

    % On définit la période symbole
    Rs = Rb / Mb;
    Ts = 1 / Rs;

    % On a donc
    Ns = floor(Ts / Te);

    % On définit un maillage temporel pour les calculs
    range = 0:Te:((M / Rb) - Te);


    % =========== MODULATION ============

    % On génère un signal discontinu à partir de ce mapping
    z_kron = kron(d, [1 zeros(1, Ns - 1)]);

    % ==================================


    % ============ EMISSION ============

    % Utilisation d'un filtre d'émission en racine de cosinus surélevé de coefficient de roll-off alpha
    alpha = 0.35;
    L = 10;
    h = rcosdesign(alpha, L, Ns);

    % On rattrape le retard induit par le filtre de mise en forme
    z_genere = filter(h, 1, [z_kron zeros(1, floor(L/2) * Ns)]);
    z_genere = z_genere((floor(L/2) * Ns) + 1:end);

    % On module le signal sur porteuse de fréquence Fp
    z_emission = z_genere;
    
    % ==================================


    % ============= CANAL ==============

    % On ajoute du bruit
    Px = mean(abs(z_emission) .^ 2);
    sigma_2 = Px * Ns / (2 * Mb * EbN0);
    bruit = sqrt(sigma_2) * (randn(1, length(z_emission)) + 1i * randn(1, length(z_emission)));
    z_bruite = z_emission + bruit;

    % ==================================


    % =========== RECEPTION ============

    % Utilisation d'un filtre de reception en racine de cosinus surélevé de coefficient de roll-off alpha
    h_r = rcosdesign(alpha, L, Ns);

    % Application du filtre
    z_reception = filter(h_r, 1, [z_bruite zeros(1, floor(L/2) * Ns)]);
    z_reception = z_reception((floor(L/2) * Ns) + 1:end);

    % Echantionnage
    z_echant = z_reception(1:Ns:end);

    z_demappe_real = sign(real(z_echant));
    z_demappe_imag = sign(imag(z_echant));

    d_demappe = z_demappe_real + 1i * z_demappe_imag;
    
    % Demapping
    z_demappe_decimal = pamdemod(z_echant', 4)';
    z_demappe_bit = int2bit(z_demappe_decimal, 2);
    
    z_demappe = zeros(1, length(bits));

    z_demappe(1:2:end) = z_demappe_bit(1,:);
    z_demappe(2:2:end) = z_demappe_bit(2,:);
    
    % ==================================
end

