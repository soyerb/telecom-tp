clear;
close all;

% Données de l'exercice
Rb = 3000;
Fe = 6000;
Fp = 2000;

% On définit le nombre de bits de l'entrée
M = 900;

% On génère l'entrée aléatoirement (2 symbole binaire)
bits = (rand(1, M) > 0.5);
%fprintf("Entrée binaire aléatoire : "); fprintf("%g", bits); fprintf("\n")

% On définit le rapport énergie par bit sur la DSP du bruit
EbN0 = 60;

% On génère la chaine de transmission
[kron, z_genere, z_emission, z_bruite, z_reception, z_echant, z_demappe, d, d_demappe, range, Ns, Mb, ~] = chaine_part2(Rb, Fe, Fp, M, bits, EbN0);

% ================= Tracé des signaux générés sur les voies en phase et en quadrature =================

figure(1)
title(['Tracé des signaux générés sur les voies en phase et en quadrature.']);

subplot(2, 1, 1)
plot(range, real(z_genere), 'r');
title("signal généré en phase");
grid
xlabel('Temps (s)')
ylabel('Signal')

subplot(2, 1, 2)
plot(range, imag(z_genere), 'b');
title("signal généré en quadrature");
grid
xlabel('Temps (s)')
ylabel('Signal')

figure(20)
plot(range, z_emission, 'r');
title(["Tracé du signal transmis sur fréquence porteuse."])
grid
xlabel('Temps (s)')
ylabel('Signal')

% ================ Tracés des constelations ==================================

% Constellation en sortie de mapping
%
% Constellation en sortie d'échantionneur, attention, z_echant est un
% vecteur de NaN si pas de bruit!
figure(2)
plot(z_echant, "bo")
hold on
plot(d, "ro", 'MarkerSize', 16, 'linewidth', 2)
grid
legend("Constellation en sortie de mapping", "Constellation en sortie d'échantionneur");
title('Tracé de la constellation en sortie de mapping')
grid on
xlabel("Re(z genere)")
ylabel("Im(z genere)")

% =====================================================================================================


% ================ Tracés de la DSP du signal sur bande de base et frequence porteuse =================

% Calcul de la densité spectrale de puissance
Sz_emission1 = pwelch(z_emission, [], [], [], Fe, 'twosided');

Fe_2 = 24000;

[kron, z_genere, z_emission, z_bruite, z_reception, z_echant, z_demappe, range, Ns, Mb, ~] = chaine(Rb, Fe_2, Fp, M, bits, EbN0);

Sz_emission2 = pwelch(real(z_emission), [], [], [], Fe_2, 'twosided');

figure(4)
title(['Tracés de la DSP du signal sur bande de base.'])
scale1 = linspace(-Fe/2, Fe/2, length(Sz_emission1));
scale2 = linspace(-Fe_2/2, Fe_2/2, length(Sz_emission2));

semilogy(scale1, fftshift(Sz_emission1), 'b');
hold on
semilogy(scale2, fftshift(Sz_emission2), 'r');
grid
legend("DSP du signal sur bande de base (Fe = 6000)", "DSP du signal sur porteuse (Fe = 24000, Fp = 2000)");
xlabel('Fréquence (Hz)')
ylabel('Réponses fréquentielles')

% =====================================================================================================


% ==================== Explication du tracé observé pour la DSP (forme, position) =====================

%{
    
%}

% =====================================================================================================


% ============== Tracé du TEB obtenu en fonction du SNR par bit à l’entrée du récepteur ===============

%fprintf("Signal réceptionné démappé : "); fprintf("%g", z_demappe); fprintf("\n")

EbN0_range = 0:1:6;
TEB = zeros(1, length(EbN0_range));
TEB_theorique = zeros(1, length(EbN0_range));

% On calcule la courbe de TEB pour EbN0 allant de 0 à 6 dB par incrément de 0.1dB
i = 1;
for EbN0 = EbN0_range
    fprintf("Calculation : %f\n", EbN0 * 100 / 6)
    % On convertit EbN0 de dB à linéaire
    EbN0lin = 10^(EbN0/10);
    [kron, z_genere, z_emission, z_bruite, z_reception, z_echant, z_demappe, d, d_demappe, range, Ns, Mb, ~] = chaine_part2(Rb, Fe, Fp, M, bits, EbN0lin);
    TEB(i) = sum(z_demappe ~= bits) / length(bits);
    TEB_theorique(i) = 1 - normcdf(sqrt(2*EbN0lin));
    i = i + 1;
end

figure(5)
title(["Tracé du TEB obtenu en fonction du SNR par bit à l’entrée du récepteur."])

semilogy(EbN0_range, TEB, 'b', EbN0_range, TEB_theorique, 'r');
legend("TEB expérimental", "TEB Théorique");
grid
xlabel('EbN0 (dB)')
ylabel('TEB')

% =====================================================================================================


% ============== Comparaison du TEB simulé avec le TEB théorique de la chaine ́etudiée =================



% =====================================================================================================
