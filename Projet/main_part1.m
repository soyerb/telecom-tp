%{
    Projet de Télécomunications
    Avril 2024, Théo Bessel et Benjamin Soyer
%}

clear;
close all;

%taille des titres
fontTitleSize = 25;
fontLabelSize = 18;


% Données de l'exercice
Rb = 3000;
Fe = 24000;
Fp = 2000;

% On définit le nombre de bits de l'entrée
M = 10000;

% On génère l'entrée aléatoirement (2 symbole binaire)
bits = (rand(1, M) > 0.5);
%fprintf("Entrée binaire aléatoire : "); fprintf("%g", bits); fprintf("\n")

% On définit le rapport énergie par bit sur la DSP du bruit
EbN0 = 0;

% On génère la chaine de transmission
[kron, z_genere, z_emission, z_bruite, z_reception, z_echant, z_demappe, range, Ns, Mb, ~] = chaine_part1(Rb, Fe, Fp, M, bits, EbN0);


% ================= Tracé des signaux générés sur les voies en phase et en quadrature =================

figure(1)
title(['Tracé des signaux générés sur les voies en phase et en quadrature.']);

subplot(2, 1, 1)
plot(range, real(z_genere), 'r');
title("signal généré en phase", "FontSize", fontTitleSize);
grid
xlabel('Temps (s)', 'FontSize', fontLabelSize)
ylabel('Signal', 'FontSize', fontLabelSize)

subplot(2, 1, 2)
plot(range, imag(z_genere), 'b');
title("signal généré en quadrature", "FontSize", fontTitleSize);
grid
xlabel('Temps (s)', 'FontSize', fontLabelSize)
ylabel('Signal', 'FontSize', fontLabelSize)

% =====================================================================================================


% ========================= Tracé du signal transmis sur fréquence porteuse ===========================

figure(2)
plot(range, z_emission, 'r');
title(["Tracé du signal transmis sur fréquence porteuse."], "FontSize", fontTitleSize)
grid
xlabel('Temps (s)', 'FontSize', fontLabelSize)
ylabel('Signal', 'FontSize', fontLabelSize)

% =====================================================================================================

% ================ Tracés de la DSP du signal sur bande de base et frequence porteuse =================

% Calcul de la densité spectrale de puissance
Sz_genere = pwelch(real(z_genere), [], [], [], Fe, 'twosided');
Sz_emission = pwelch(real(z_emission), [], [], [], Fe, 'twosided');

figure(3)
title(['Tracés de la DSP du signal sur bande de base et frequence porteuse.'], 'FontSize', fontTitleSize);
scale = linspace(-Fe/2, Fe/2, length(Sz_genere));

subplot(2, 1, 1)
semilogy(scale, fftshift(Sz_genere), 'b');
grid
xlabel('Fréquence (Hz)', 'FontSize', fontLabelSize)
ylabel('Réponses fréquentielles', "FontSize", fontLabelSize)
title("DSP du signal sur bande de base", "FontSize", fontTitleSize)

subplot(2, 1, 2)
semilogy(scale, fftshift(Sz_emission), 'r');
grid
xlabel('Fréquence (Hz)', 'FontSize', fontLabelSize)
ylabel('Réponses fréquentielles', 'FontSize', fontLabelSize)
title("DSP du signal sur frequence porteuse.", "FontSize", fontTitleSize)

% =====================================================================================================


% ==================== Explication du tracé observé pour la DSP (forme, position) =====================

%{
    Nous avons tracé la densité spectrale de puissance de notre signal avant d'avoir été translater sur une 
    fréquence porteuse et après cette translation.
    Initialement, nous constatons par nos tracés que dans la majeure partie de la puissance est centré autour 
    de f = 0Hz, chose qui rentre en accord avec la théorie car initialement le signal est sur bande de base
    Il est donc normal de retrouver cette forme pour la DSP du signal avant la multiplication par
    l'exponentielle complexe.
    On prends la partie réel du signal pour en calculer sa DSP. Ainsi on fait le produit entre notre signal et un
    cosinus de la forme cos(2pi*fp*t) avec ici fp = 2000Hz. Ainsi lorsqu'on passe à la transformé de fourier, on fait le produit de
    convolution entre notre signal en fréquentiel et 2 dirac en fp et -fp.
    Ce résultat théorique est retrouvé dans le tracé expérimentale de la DSP du signal après transposition sur
    fréquence porteuse comme on peut le voir sur la figure ??
    La forme de la DSP est aussi analysable. notre DSP est le module au carré d'un cosinus hyperbolique de par le fait que 
    nous utilisons des filtres en racine de cosinus surélevés. Ainsi il est logique de retrouver comme résultat notre courbe
    expérimentale.
%}

% ====================================================================================================


% ============== Tracé du TEB obtenu en fonction du SNR par bit à l’entrée du récepteur ===============

%fprintf("Signal réceptionné démappé : "); fprintf("%g", z_demappe); fprintf("\n")

EbN0_range = 0:0.1:6;
TEB = zeros(1, length(EbN0_range));
TEB_theorique = zeros(1, length(EbN0_range));

% On calcule la courbe de TEB pour EbN0 allant de 0 à 6 dB par incrément de 0.1dB
i = 1;
for EbN0 = EbN0_range
    fprintf("Calculation : %f\n", EbN0 * 100 / 6)
    % On convertit EbN0 de dB à linéaire
    EbN0lin = 10^(EbN0/10);
    [kron, z_genere, z_emission, z_bruite, z_reception, z_echant, z_demappe, range, Ns, Mb, ~] = chaine_part1(Rb, Fe, Fp, M, bits, EbN0lin);
    TEB(i) = sum(z_demappe ~= bits) / length(bits);
    TEB_theorique(i) = 1 - normcdf(sqrt(2*EbN0lin));
    i = i + 1;
end

figure(5)
semilogy(EbN0_range, TEB, 'b', EbN0_range, TEB_theorique, 'r');
title(["Tracé du TEB obtenu en fonction du SNR par bit à l'entrée du récepteur."], "FontSize", fontTitleSize)
legend("TEB expérimental", "TEB Théorique");
grid
xlabel('EbN0 (dB)', 'FontSize', fontLabelSize)
ylabel('TEB', 'FontSize', fontLabelSize)

% =====================================================================================================


% ============== Comparaison du TEB simulé avec le TEB théorique de la chaine ́etudiée =================



% =====================================================================================================