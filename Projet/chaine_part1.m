%{
    Projet de Télécomunications
    Avril 2024, Théo Bessel et Benjamin Soyer
%}

%{
    Chaine de transmission 1 :
    – Mapping : symboles binaires à moyenne nulle.
    – Filtre de mise en forme de réponse impulsionnelle rectangulaire de hauteur 1 et de durée  ́egale à la période symbole.

    Variables :
    - z_genere : Le signal généré à partir du mapping, avant modulation sur fréquence porteuse
    - z_emission : Le signal en sortie du modulateur, qui va être émis
    - z_bruite : Le signal à la fin du canal de transmission donc avec du bruit rajouté
    - z_reception : Le signal en sortie du récepteur (donc en sortie de la chaine de transmission)
    - z_echant : Le signal après échantillonage
    - z_demappe : Le signal après démapping
%}

function [kron, z_genere, z_emission, z_bruite, z_reception, z_echant, z_demappe, range, Ns, Mb, Rs, Ts] = chaine_part1(Rb, Fe, Fp, M, bits, EbN0)
    % ============ MAPPING ============

    % On effectue le mapping
    % Pour `a` on prend les bits d'indices impairs
    % Pour `b` on prend les bits d'indices pairs
    a = 2 * bits(1:2:end) - 1;
    b = 2 * bits(2:2:end) - 1;
    d = a + 1i * b;

    % ==================================

    
    % On définit la période d'échantillonage
    Te = 1 / Fe;

    % Le nombre de bits par symboles est donc
    Mb = M / length(d);

    % On définit la période symbole
    Rs = Rb / Mb;
    Ts = 1 / Rs;

    % On a donc
    Ns = floor(Ts / Te);

    % On définit un maillage temporel pour les calculs
    range = 0:Te:((M / Rb) - Te);


    % =========== MODULATION ============

    % On génère un signal discontinu à partir de ce mapping
    kron = zeros(1, length(range));
    for i = 0:(length(d) - 1)
        kron(i * Ns + 1) = d(i + 1);
    end

    % ==================================


    % ============ EMISSION ============

    % Utilisation d'un filtre d'émission en racine de cosinus surélevé de coefficient de roll-off alpha
    alpha = 0.35;
    L = 10;
    h = rcosdesign(alpha, L, Ns);

    % On rattrape le retard induit par le filtre de mise en forme
    z_genere = filter(h, 1, [kron zeros(1, floor(L/2) * Ns)]);
    z_genere = z_genere((floor(L/2) * Ns):end-1);

    % On module le signal sur porteuse de fréquence Fp
    z_emission = exp(2 * 1i * pi * Fp * range) .* z_genere;
    
    % ==================================


    % ============= CANAL ==============

    % On ajoute du bruit
    Px = mean(abs(z_emission) .^ 2);
    sigma_2 = Px * Ns / (Mb * EbN0);
    bruit = sqrt(sigma_2) * randn(1, length(z_emission));
    z_bruite = z_emission + bruit;

    % ==================================


    % =========== RECEPTION ============

    % Utilisation d'un filtre de reception en racine de cosinus surélevé de coefficient de roll-off alpha
    h_r = rcosdesign(alpha, L, Ns);
    
    % Multiplication du signal par cos et sin pour récupérer la phase et la quadrature
    z_voie_I = cos(2 * pi * Fp * range) .* z_bruite;
    z_voie_J = sin(2 * pi * Fp * range) .* z_bruite;

    % Application du filtre pour les deux voies
    z_reception_voie_I = filter(h_r, 1, [z_voie_I zeros(1, floor(L/2) * Ns)]);
    z_reception_voie_J = filter(h_r, 1, [z_voie_J zeros(1, floor(L/2) * Ns)]);

    % Somme des voies
    z_reception = z_reception_voie_I - 1i * z_reception_voie_J;
    z_reception = z_reception((floor(L/2) * Ns)+1:end);
    
    % On échantionne le signal après réception
    n0 = Mb;

    z_echant = zeros(1, length(z_reception));
    for i = n0:Ns:length(z_reception)
        z_echant(i:(i + Ns - 1)) = z_reception(i);
    end

    z_demappe_real = real(z_echant(Ns:Ns:end)) > 0;
    z_demappe_imag = imag(z_echant(Ns:Ns:end)) > 0;

    z_demappe = zeros(1, length(bits));
    for i = 1:floor(length(bits) / 2)
        z_demappe(2 * i - 1:2 * i) = [z_demappe_real(i) z_demappe_imag(i)];
    end
    
    % ==================================
end