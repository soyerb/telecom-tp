%{
    TP de Télécomunications
    Avril 2024, Théo Bessel
%}

clear;
close all;

% Données de l'exercice
Rb = 3000;
Fe = 24000;

% On définit le nombre de bits de l'entrée
M = 4000;

% On génère l'entrée aléatoirement
bits = (rand(1,M) > 0.5);

fprintf("Entrée binaire aléatoire : "); fprintf("%g", bits); fprintf("\n")

%{
    On appelle les trois modulateurs :
%}

[kron1, z1, Sz1, range1, Ns1, ~] = modulateur1(Rb, Fe, M, bits);
[kron2, z2, Sz2, range2, Ns2, ~] = modulateur2(Rb, Fe, M, bits);
[kron3, z3, Sz3, range3, Ns3, ~] = modulateur3(Rb, Fe, M, bits);

% Tracé de la figure pour le modulateur 1 :
figure
subplot(3,1,1)
plot(range1, kron1, 'g');
hold on
plot(range1, z1, 'r');
grid
xlabel('Temps (s)')
ylabel('Signal')
title(['Tracé du signal généré pour le modulateur 1']);
ylim([-2 2])

% Tracé de la figure pour le modulateur 2 :
subplot(3,1,2)
plot(range1, kron2, 'g');
hold on
plot(range1, z2, 'r');
grid
xlabel('Temps (s)')
ylabel('Signal')
title(['Tracé du signal généré pour le modulateur 2']);
ylim([-4 4])

% Tracé de la figure pour le modulateur 3 :
subplot(3,1,3)
plot(range3, kron3, 'g');
hold on
plot(range3, z3, 'r');
grid
xlabel('Temps (s)')
ylabel('Signal')
title(['Tracé du signal généré pour le modulateur 3']);
ylim([-2 2])

% On trace les DSP des signaux :

figure
scale1=linspace(-Fe/2,Fe/2,length(Sz1));
semilogy(scale1,fftshift(Sz1), 'r')
hold on
scale2=linspace(-Fe/2,Fe/2,length(Sz2));
semilogy(scale2,fftshift(Sz2), 'g')
hold on
scale3=linspace(-Fe/2,Fe/2,length(Sz3));
semilogy(scale3,fftshift(Sz3), 'b')
xlabel('Fréquences (Hz)')
ylabel('DSP')
title('Tracé de la DSP des singaux générés');
legend({'Modulateur 1', 'Modulateur 2', 'Modulateur 3'}, 'Location', 'southwest')

eyediagram(z1(Ns1:end),Ns1,Ns1,Ns1-1)
eyediagram(z2(Ns2:end),Ns2,Ns2,Ns2-1)
eyediagram(z3(Ns3:end),Ns3,Ns3,Ns3-1)