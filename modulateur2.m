%{
    TP de Télécomunications
    Avril 2024, Théo Bessel
%}

%{
    Modulateur 2 :
    – Mapping : symboles 4-aires à moyenne nulle.
    – Filtre de mise en forme de réponse impulsionnelle rectangulaire de hauteur 1 et de durée  ́egale à la période symbole.
%}

function [kron, z, Sz, range, Ns, Mb, Rs, Ts] = modulateur2(Rb, Fe, M, bits)
    % On définit la période d'échantillonage
    Te = 1/Fe;

    % On effectue le mapping
    % On a donc la correspondance suivante :
    % 00 -> 3
    % 11 -> -1
    % 01 -> 1
    % 10 -> -3
    a = zeros(1, length(bits)/2);
    for i=1:length(a)
        if bits(2*i-1) == 0
            if bits(2*i) == 0
                a(i) = 3;
            else
                a(i) = 1;
            end
        else
            if bits(2*i) == 0
                a(i) = -3;
            else
                a(i) = -1;
            end
        end
    end
    fprintf("Mapping du modulateur 2 : "); fprintf("%g", a); fprintf("\n")

    % Le nombre de bits par symboles est donc
    Mb = M/length(a);

    % On définit la période symbole
    Rs = Rb/Mb;
    Ts = 1/Rs;

    % On a donc
    Ns = floor(Ts/Te);

    % On génère un signal discontinu à partir de ce mapping
    range = 0:Te:M/Rb-Te;
    kron = zeros(1, length(range));

    for i=0:length(a)-1
        kron(i*Ns+1) = a(i+1);
    end

    % On génère ensuite un filtre
    t = 1:Ns;
    h = ones(1, length(t));

    z = filter(h, 1, kron);

    Sz=pwelch(z, [], [], [], Fe, 'twosided');
end